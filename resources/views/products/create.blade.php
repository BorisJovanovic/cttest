@extends('layout')

    @section('content')


        <div class="container-fluid">
            <div class="row">

                <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">

                    <form id="productForm" action="/store" method="post" role="form">
                        <legend>Add new product</legend>

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="product_name">Product Name</label>
                            <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Add product name" required>
                        </div>

                        <div class="form-group">
                            <label for="quantity">Quantity</label>
                            <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Add product qunatity" required>
                        </div>

                        <div class="form-group">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" name="price" id="price" placeholder="Add product price" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>

                        <div class="infobox">

                        </div>
                    </form>


                </div>

                <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                    <h3>Products</h3>

                    <div id="product-list">

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Quantity</th>
                                    <th>Price per Item</th>
                                    <th>Added On</th>
                                    <th>Sum</th>
                                </tr>
                                </thead>
                                <tbody id="products">

                                @if(!empty($productData))

                                    @php($total = 0)

                                    @foreach($productData as $product)
                                        <tr class="product">
                                            <td>{{ $product->product_name }}</td>
                                            <td>{{ $product->quantity }}</td>
                                            <td>$ {{ $product->price }}</td>
                                            <td>{{ $product->timestamp }}</td>
                                            <td>$ {{ $product->total }}</td>
                                        </tr>

                                        @php($total += $product->total)

                                        @endforeach

                                    @endif

                                    <tr id="productsTotal">
                                        <td colspan="4"><b>Total Product Amount</b></td>
                                        <td>$ {{ isset($total) ? $total : "0" }}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>


            </div>
        </div>


        <script>
            $(document).ready(function(){

                $('button[type="submit"]').click(function(){

                    var data = $('#productForm').serialize();
                    $('.infobox').html('');

                    $.ajax({
                       type: "POST",
                        data: data,
                        url: "/store",
                        success: function(response) {

                            $('tbody#products tr.product').remove();
                            $('tbody#products tr#productsTotal td:last-child').html('');

                            var total = 0;
                            var tableRows = '';

                            if (response.message) {

                            }


                            $.each(response, function(i){

                                tableRows += '<tr class="product">';
                                tableRows += '<td>' + response[i].product_name + '</td>';
                                tableRows += '<td>' + response[i].quantity + '</td>';
                                tableRows += '<td>$ ' + response[i].price + '</td>';
                                tableRows += '<td>' + response[i].timestamp + '</td>';
                                tableRows += '<td>$ ' + response[i].total + '</td>';
                                tableRows += '<tr>';

                                total += response[i].total;

                            });

                            $('tbody#products').prepend(tableRows);
                            $('tbody#products tr#productsTotal td:last-child').html('$ ' + total);

                        },
                        error: function(error) {

                           var response = error.responseJSON;

                           if (response.errors) {

                               var errorMessage = '';

                                $.each(response.errors, function(e, value){
                                    errorMessage += '<p>' + e + ': ' + value + '</p>';
                                });
                           }

                           $('.infobox').append(errorMessage);
                        }

                    });

                    return false;
                });


            });
        </script>


    @endsection