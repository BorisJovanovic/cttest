<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productData = $this->show();

//        dd($productData);

        return view('products.create', compact('productData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $newProduct = $this->validateProduct();

        if ($this->ifJsonExists()) {
            $products = $this->getProductsFromJson();
        } else {
            $products = [];
        }

        $newProduct['timestamp'] = $this->setProductTimeStamp();

        $product = (object) $newProduct;

        array_push($products, $product);

        Storage::disk('local')->put('data.json', json_encode($products));

        $productData = $this->show();

        return $productData;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if ($this->ifJsonExists()) {
            $products = $this->getProductsFromJson();
        } else {
            $products = [];
        }

        foreach ($products as &$product) {
            $product->total = $this->getTotalAmountForOneProduct($product);
        }

       return $products;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function getTotalAmountForOneProduct($product)
    {
        return $product->quantity * $product->price;
    }

    public function getProductsFromJson()
    {
        return json_decode(Storage::disk('local')->get('data.json'));
    }

    public function ifJsonExists()
    {
        return Storage::disk('local')->exists('data.json');
    }

    public function validateProduct()
    {
        return request()->validate([
            'product_name' => 'required|string|max:100',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric'
        ]);
    }

    public function setProductTimeStamp()
    {
        return $timestamp = date('Y:m:d, h:i:s');
    }

}
